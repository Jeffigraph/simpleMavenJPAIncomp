package org.formation.spring.test;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test_TP5 {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);
		service.listClients();
		service.addClient(
				new Client("albert", "legrand", "bebert", "bebert", new Adresse(12, "rue du vent", "Strasbourg")));
		service.addClient(
				new Client("Philippe", "Maxwell", "max", "max", new Adresse(56, "rue des fleurs", "Marseilles")));
		// ajouter client
		// tester
		System.out.println(service.listClients().get(0));
		System.err.println(service.listClients().get(1));

		((ConfigurableApplicationContext) (context)).close();
	}

}
