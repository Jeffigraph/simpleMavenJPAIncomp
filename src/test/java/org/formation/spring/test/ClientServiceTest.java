package org.formation.spring.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class ClientServiceTest {
	// nom methode = 'findclient doit rendre un client'

	@Autowired
	private IPrestiBanqueService service;

	@Autowired
	private CrudClientDAO dao;

	@Test
	public void findAllClientByNomReturnTheCorrespondingClient() {
		assertNotNull("dao is null", dao);
		service.addClient(
				new Client("albert", "legrand", "bebert", "bebert", new Adresse(12, "rue du vent", "Strasbourg")));
		List<Client> lstClients = dao.findAllClientByNom("albert");

		assertTrue("nothing found", lstClients.size() > 0);

		for (Client client : lstClients) {
			if ("albert".equals(client.getNom())) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
		}
	}

	@Test
	public void listClientReturnAListOfAllCLients() {
		assertNotNull("Service Injection failed", service);

		List<Client> lstClientsInit = service.listClients();

		service.addClient(
				new Client("albert", "legrand", "bebert", "bebert", new Adresse(12, "rue du vent", "Strasbourg")));
		service.addClient(
				new Client("Philippe", "Maxwell", "max", "max", new Adresse(56, "rue des fleurs", "Marseilles")));

		List<Client> lstClientsFinal = service.listClients();

		assertTrue("list init:" + lstClientsInit.size() + " list final:" + lstClientsFinal.size(),
				lstClientsInit.size() + 2 == lstClientsFinal.size());
	}

	@Test
	public void chercherClientsReturnAListOfClientsWithTheNameGivenInParameter() {
		assertNotNull("Service Injection failed", service);

		service.addClient(
				new Client("albert", "legrand", "bebert", "bebert", new Adresse(12, "rue du vent", "Strasbourg")));

		List<Client> lstClientTrouves = service.chercherClients("albert");

		assertTrue(lstClientTrouves.size() > 0 && ("albert".equals(lstClientTrouves.get(0).getNom())));
	}

	@Test
	public void addClientMustSaveTheCLientIntoTheDatabase() {
		assertNotNull("Service Injection failed", service);

		service.addClient(
				new Client("Morris", "Philippe", "bebert", "bebert", new Adresse(12, "rue du vent", "Strasbourg")));

		List<Client> lstClientTrouves = service.chercherClients("Morris");

		assertTrue(lstClientTrouves.size() > 0);
	}

}
